import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalController, ToastController } from '@ionic/angular';
import { DetailsComponent } from '../details/details.component';
import { Item } from '../interfaces/item.class';
import { DataService } from '../services/data.service';
import { IonicAuthService } from '../services/ionic-auth.service';
import { SettingsComponent } from '../settings/settings.component';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/compat/firestore';
import { FILE } from '../interfaces/file.class';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.page.html',
  styleUrls: ['./catalog.page.scss'],
})
export class CatalogPage implements OnInit {
  fotos = [];
  Items = [];
  userDetail: string;
  files: Observable<FILE[]>;
  id: String;
  basket: Item[] = [];
  userData: Item = {} as Item;
  private ngFirestoreCollection: AngularFirestoreCollection<FILE>;

  constructor(private dataService: DataService, 
    public toastController: ToastController, 
    public modalController: ModalController,
    private ionicAuthService: IonicAuthService,
    private router: Router,
    private angularFirestore: AngularFirestore,
    private angularFireStorage: AngularFireStorage,
    private activatedRoute: ActivatedRoute) 
    
    {
    this.ionicAuthService.userDetails().subscribe(response => {
      if (response !== null) {
        let localdata = JSON.parse(atob(localStorage.getItem('user')));
        if(response.email == localdata.email){
            console.log('Loged In User');
          }else{
            localStorage.removeItem('user');
            this.router.navigateByUrl('login');
          }
      } else {
        this.router.navigateByUrl('login');
      }
    }, error => {
      console.log(error);
    });

    this.ngFirestoreCollection = angularFirestore.collection<FILE>('fotitos');
    this.files = this.ngFirestoreCollection.valueChanges();
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    console.log(this.files)
    
    this.Items.forEach(element => {
      console.log(element)
      this.fotos.forEach(img => {
        if(element.id == img.idItem){
          element.foto = img.idItem;
          console.log("imagen si")
        }else {
          element.foto = "https://www.bandg.com/assets/img/default-product-img.png?w=400&h=225&scale=both&mode=max";
          console.log("imagen no")
        }
      })
      
    });
  }
  
  ngOnInit() {
    let localdata = JSON.parse(atob(localStorage.getItem('user')));
    this.userDetail = localdata;
    this.dataService.list_item().subscribe(
      res => {
        this.Items = res.map((item) => {
          return {
            id: item.payload.doc.id,
            ... item.payload.doc.data() as Item
          };
        })
      }
    );

  }

  async presentModal(id, category, name, description, fotos) {
    const modal = await this.modalController.create({
      component: DetailsComponent,
      componentProps: {id,category,name, description, fotos}
    });
    modal.present();
  }


  async addbasket(Item){
    const toast = await this.toastController.create({  
      message: "Producte afegit al carro",   
      duration: 1000  
    });  
    toast.present();
    this.basket.push(Item);
    console.log(this.basket);
    localStorage.setItem("basket", JSON.stringify(this.basket))
    
  }

  async presentModalprofile() {
    const modal = await this.modalController.create({
      component: SettingsComponent,
      componentProps: {}
    });
    modal.present();
  }

  checkout(){
    this.router.navigateByUrl('checkout');
  }
  
}