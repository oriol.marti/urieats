import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Map } from 'mapbox-gl'
import * as mapboxgl from 'mapbox-gl';
import { EmailComposer } from '@awesome-cordova-plugins/email-composer/ngx';
import { Router } from '@angular/router';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.page.html',
  styleUrls: ['./checkout.page.scss'],
})
export class CheckoutPage implements OnInit {
  basket: any;
  mapbox = (mapboxgl as typeof mapboxgl);
  map: mapboxgl.Map;
  style = `mapbox://styles/mapbox/streets-v11`;
  // Coordenadas de la localización donde queremos centrar el mapa
  lat = 43.1746;
  lng = -2.4125;
  zoom = 15;
  userDetail: any;
  constructor(private emailComposer: EmailComposer,private router: Router,) {
    this.mapbox.accessToken = environment.mapBoxToken;    
   }

  ngOnInit(): void {
    this.basket = JSON.parse(localStorage.getItem("basket"))
    console.log(this.basket);

    this.map = new mapboxgl.Map({
      container: 'map',
      style: this.style,
      zoom: this.zoom,
      center: [this.lng, this.lat]
    });
    this.map.addControl(new mapboxgl.NavigationControl());

    this.map.on('load', () => {
      this.map.resize();
    });
    let localdata = JSON.parse(atob(localStorage.getItem('user')));
    this.userDetail = localdata;
  }
  
  endEmail(){
    let email = {
      to: this.userDetail['role'], cc: 'oriol.marti.7e5@itb.cat', bcc: [],
      subject: 'Urieats',
      body: 'La seva comanda esta confirmada', isHtml: true
    }
    this.emailComposer.open(email);
  }

  finish(){
    alert("COMANDA ARCHIVADA")
    localStorage.removeItem('basket');
    this.router.navigateByUrl('dashboard');
  }
  
 

}
