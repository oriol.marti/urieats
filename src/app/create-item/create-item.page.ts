import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-create-item',
  templateUrl: './create-item.page.html',
  styleUrls: ['./create-item.page.scss'],
})
export class CreateItemPage implements OnInit {

  itemForm: FormGroup;

 constructor(private dataService: DataService,
   public formBuilder: FormBuilder,   
   private router: Router) { }

 ngOnInit() {
    this.itemForm = this.formBuilder.group({
      name: [''],
      category: [''],
      description: [''],
    })
  }

  onSubmit() {
    if (!this.itemForm.valid) {
      return false;
    } else {
      this.dataService.cr_item(this.itemForm.value)
      .then(() => {
        this.itemForm.reset();
        this.router.navigate(['list-item']);
      }).catch((err) => {
        console.log(err)
      });
    }
  }
 
}
