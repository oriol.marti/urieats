import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { ModalController } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { IonicAuthService } from '../services/ionic-auth.service';
import { SettingsComponent } from '../settings/settings.component';

declare var mapboxgl: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})

export class DashboardPage implements OnInit {
  userDetail: string;

  constructor(
    private router: Router,
    private ionicAuthService: IonicAuthService,
    public modalController: ModalController,
  ) {
    this.ionicAuthService.userDetails().subscribe(response => {
      if (response !== null) {
        let localdata = JSON.parse(atob(localStorage.getItem('user')));
        if(response.email == localdata.email){
            console.log('Loged In User');
          }else{
            localStorage.removeItem('user');
            this.router.navigateByUrl('login');
          }
      } else {
        this.router.navigateByUrl('login');
      }
    }, error => {
      console.log(error);
    })
  }
  ngOnInit() {
    let localdata = JSON.parse(atob(localStorage.getItem('user')));
    this.userDetail = localdata;
  }

  signOut() {
    this.ionicAuthService.signoutUser()
      .then(res => {
        this.router.navigateByUrl('login');
      })
      .catch(error => {
        console.log(error);
      })
  }
  comprar(){
    this.router.navigateByUrl('catalog');
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: SettingsComponent,
      componentProps: {}
    });
    modal.present();
  }

  manageProducts(){
    this.router.navigateByUrl('list-item');
  }

  checkrole(){
    if(this.userDetail['role'] == "Admin"){
      return true
    }else {return false}
  }
}




