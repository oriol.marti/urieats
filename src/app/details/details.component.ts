import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
})
export class DetailsComponent implements OnInit {
  @Input() id: string;
  @Input() foto: string;
  @Input() name: string;
  @Input() description: string;
  

  constructor(public modalController: ModalController) { }

  ngOnInit() {}

  async canDismiss() {
    this.modalController.dismiss();
  }

}
