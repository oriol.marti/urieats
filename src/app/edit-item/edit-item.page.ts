import { Component, OnInit } from '@angular/core';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/compat/storage';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../services/data.service';
import { Observable } from 'rxjs';
import { finalize, tap } from 'rxjs/operators';
import { FILE } from '../interfaces/file.class';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/compat/firestore';

@Component({
  selector: 'app-edit-item',
  templateUrl: './edit-item.page.html',
  styleUrls: ['./edit-item.page.scss'],
})
export class EditItemPage implements OnInit {

  editForm: FormGroup;
  id: String;

  ngFireUploadTask: AngularFireUploadTask;

  progressNum: Observable<number>;

  progressSnapshot: Observable<any>;

  fileUploadedPath: Observable<string>;

  files: Observable<FILE[]>;

  FileName: string;
  FileSize: number;

  isImgUploading: boolean;
  isImgUploaded: boolean;

  private ngFirestoreCollection: AngularFirestoreCollection<FILE>;
  
  constructor(private dataService: DataService, private activatedRoute: ActivatedRoute,
        private router: Router, public formBuilder: FormBuilder,private angularFirestore: AngularFirestore,private angularFireStorage: AngularFireStorage) 
        {
          this.isImgUploading = false;
          this.isImgUploaded = false;
          
          this.ngFirestoreCollection = angularFirestore.collection<FILE>('fotitos');
          this.files = this.ngFirestoreCollection.valueChanges();
          this.id = this.activatedRoute.snapshot.paramMap.get('id');
          this.dataService.getid_item(this.id).subscribe(
            res => {
              this.editForm = this.formBuilder.group({
                name: [res['name']],
                category: [res['category']],
                description: [res['description']]
            })
        });
  }
  fileStorage(image: FILE) {
    const ImgId = this.angularFirestore.createId();
    
    this.ngFirestoreCollection.doc(ImgId).set(image).then(data => {
      console.log(data);
    }).catch(error => {
      console.log(error);
    });
}  

  fileUpload(event: FileList) {
      
    const file = event.item(0)

    if (file.type.split('/')[0] !== 'image') { 
      console.log('File type is not supported!')
      return;
    }

    this.isImgUploading = true;
    this.isImgUploaded = false;

    this.FileName = file.name;

    const fileStoragePath = `filesStorage/${new Date().getTime()}_${file.name}`;

    const imageRef = this.angularFireStorage.ref(fileStoragePath);

    this.ngFireUploadTask = this.angularFireStorage.upload(fileStoragePath, file);

    this.progressNum = this.ngFireUploadTask.percentageChanges();
    this.progressSnapshot = this.ngFireUploadTask.snapshotChanges().pipe(
      
      finalize(() => {
        this.fileUploadedPath = imageRef.getDownloadURL();
        
        this.fileUploadedPath.subscribe(resp=>{
          this.fileStorage({
            name: file.name,
            filepath: resp,
            idItem: this.id,
            size: this.FileSize
          });
          this.isImgUploading = false;
          this.isImgUploaded = true;
        },error => {
          console.log(error);
        })
      }),
      tap(snap => {
          this.FileSize = snap.totalBytes;
      })
    )
}

 
ngOnInit() {
  this.editForm = this.formBuilder.group({
      name: [''],
      category: [''],
      description: ['']
  })
}

onSubmit() {
  this.dataService.up_item(this.id, this.editForm.value);
  this.router.navigateByUrl('list-item');
}

}
