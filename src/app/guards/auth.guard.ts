import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, CanDeactivate, CanLoad, Route, Router, RouterStateSnapshot, UrlSegment, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { IonicAuthService } from '../services/ionic-auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanLoad {

    constructor(public authService: IonicAuthService,
      public router: Router){ }
  
    canLoad(): boolean {
      const result = this.authService.isLoggedIn
      if(!result){
        this.router.navigateByUrl('/login');
      }
      return this.authService.isLoggedIn;
    }
}
