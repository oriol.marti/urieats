export interface User {
    email: string;
    displayName: string;
    role: string;
 }
