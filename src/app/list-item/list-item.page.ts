import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { Item } from '../interfaces/item.class';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.page.html',
  styleUrls: ['./list-item.page.scss'],
})
export class ListItemPage implements OnInit {

  Items = [];
 constructor(private dataService: DataService,private router: Router,) { }
 ngOnInit() {
   this.dataService.list_item().subscribe(
     res => {
       this.Items = res.map((item) => {
         return {
           id: item.payload.doc.id,
           ... item.payload.doc.data() as Item
         };
       })
     }
   );
 }
 deleteItem(id){
  if (window.confirm('Do you want to delete this Item?')) {
    this.dataService.dl_item(id)
  }
}

return(){
  this.router.navigateByUrl('dashboard');
}

create(){
  this.router.navigateByUrl('create-item');
}
}
