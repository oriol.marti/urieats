import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { IonicAuthService } from '../services/ionic-auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})

export class RegisterPage implements OnInit {

  userForm: FormGroup;
  successMsg: string = '';
  errorMsg: string = '';


  error_msg = {
    'username': [
      { 
        type: 'required', 
        message: 'Provide name.' 
      },
      { 
        type: 'pattern', 
        message: 'name is not valid.' 
      }
    ],
    'email': [
      { 
        type: 'required', 
        message: 'Provide email.' 
      },
      { 
        type: 'pattern', 
        message: 'Email is not valid.' 
      }
    ],
    'password': [
      { 
        type: 'required', 
        message: 'Password is required.' 
      },
      { 
        type: 'minlength', 
        message: 'Password length should be 6 characters long.' 
      }
    ],
    'role': [
      { 
        type: 'required', 
        message: 'Select a role.' 
      }
    ],
    };

  constructor(
    private router: Router,
    private ionicAuthService: IonicAuthService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.userForm = this.fb.group({
      username: new FormControl('', Validators.compose([
        Validators.minLength(6),
        Validators.required
      ])),
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.minLength(6),
        Validators.required
      ])),
      role: new FormControl('', Validators.compose([
        Validators.required
      ])),
    });
  }


  signUp(value) {
    //this.ionicAuthService.createUser(value)
    this.ionicAuthService.registerUser(value)
      .then((response) => {
        this.errorMsg = "";
        this.successMsg = "New user created.";
        this.router.navigateByUrl('login');
      }, error => {
        this.errorMsg = error.message;
        this.successMsg = "";
      })
  }

  goToLogin() {
    this.router.navigateByUrl('login');
  }

}
