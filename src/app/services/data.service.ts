import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Router } from '@angular/router';
import { Item } from '../interfaces/item.class';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private firestore: AngularFirestore, private router: Router) { }

  cr_item(item: Item){ //crea un nou item
    return this.firestore.collection('item').add(item);
  }
  list_item(){
    return this.firestore.collection('item').snapshotChanges();
  }
  getid_item(id){
    return this.firestore.collection('item').doc(id).valueChanges();
  }
  getid_item_img(id_item){
     const data = this.firestore.collection('fotitos',ref => ref.where('idItem','==',id_item));
     return(data).snapshotChanges();
  }
  up_item(id, item: Item){
    this.firestore.collection('item').doc(id).update(item)
     .then(() => {
       this.router.navigate(['list_item']);
     }).catch(error => console.log(error));
  }
  dl_item(id){
    this.firestore.doc('item/'+id).delete();
  }

}
