import { Injectable } from '@angular/core'
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { User } from '../interfaces/users.class';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/compat/firestore';
import { Router } from '@angular/router';
import { getAuth } from "firebase/auth";
import { AuthStorageService } from './auth-storage.service';

@Injectable({
  providedIn: 'root'
})

export class IonicAuthService {
  userstatus: boolean;
  uservalue: any;
  userData: User = {} as User;
  auth = getAuth();


  constructor(
    private angularFireAuth: AngularFireAuth,public afStore: AngularFirestore,
    public ngFireAuth: AngularFireAuth, public router: Router,  private userDataStorage: AuthStorageService) {
      this.ngFireAuth.authState.subscribe(user => {
        if (user) {
          this.userDataStorage.getUser(this.auth.currentUser.email).subscribe(
             user => {  this.userData = {
                email: this.auth.currentUser.email,
                displayName: user['displayName'],
                role: user['role']};
                localStorage.setItem('userNEncripted', JSON.stringify(this.userData));
                localStorage.setItem('user', btoa(JSON.stringify(this.userData)));
               }
           ); 
          }else {
            localStorage.setItem('user', null);
      }
      })
    }


  createUser(value: any) {
    return new Promise<any>((resolve, reject) => {
      this.angularFireAuth.createUserWithEmailAndPassword(value.email, value.password)
        .then(
          res => resolve(res),
          err => reject(err))
    })
  }

  registerUser(value: any) {
    return this.ngFireAuth.createUserWithEmailAndPassword(value.email, value.password)
    .then((result) => {
        const userData: User = {
        email: value.email,
        displayName: value.username,
        role: value.role,
      }
      this.setUserData(userData);
    });
  }

  setUserData(user) {
    const userRef: AngularFirestoreDocument<any> = this.afStore.doc(`users/${user.email}`);
    const userData: User = {
      email: user.email,
      displayName: user.displayName,
      role: user.role,
    }
    return userRef.set(userData)
  }

  signinUser(value: any) {
    return new Promise<any>((resolve, reject) => {
      this.angularFireAuth.signInWithEmailAndPassword(value.email, value.password) 
        .then(
          res => resolve(res),
          err => reject(err))
    })
  }

  signoutUser() {
    return new Promise<void>((resolve, reject) => {
      if (this.angularFireAuth.currentUser) {
        this.angularFireAuth.signOut()
          .then(async () => {
            console.log("Sign out");
            resolve();
            await this.ngFireAuth.signOut();
            localStorage.removeItem('user');
            this.router.navigate(['login']);
          }).catch(() => {
            reject();
          });
      }
    })
  }

  userDetails() {
    return this.angularFireAuth.user
  }
  
  get isLoggedIn(): boolean {
    //const user = JSON.parse(atob(localStorage.getItem('user')));
    const user = localStorage.getItem('user');
    return (user !== null) ? true : false;
  }

 

}