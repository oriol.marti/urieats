import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { User } from '../interfaces/users.class';
import { IonicAuthService } from '../services/ionic-auth.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent implements OnInit {
  userDetail: User;
  public title: string;
  public description: string;


  constructor(public modalController: ModalController,
    private ionicAuthService: IonicAuthService,
    private router: Router,
    ) 
    {
      this.ionicAuthService.userDetails().subscribe(response => {
        if (response !== null) {
          let localdata = JSON.parse(atob(localStorage.getItem('user')));
          if(response.email == localdata.email){
              console.log('Loged In User');
            }else{
              localStorage.removeItem('user');
              this.router.navigateByUrl('login');
            }
        } else {
          this.router.navigateByUrl('login');
        }
      }, error => {
        console.log(error);
      })
    }

  ngOnInit(){

    let localdata = JSON.parse(atob(localStorage.getItem('user')));
    this.userDetail = localdata;

  }

  signOut() {
    this.ionicAuthService.signoutUser()
      .then(res => {
        this.router.navigateByUrl('login');
      })
      .catch(error => {
        console.log(error);
      })
  }
  
  async canDismiss() {
    this.modalController.dismiss();
  }

  slcBTN(){
    this.router.navigateByUrl('lenguages');
  }

}
